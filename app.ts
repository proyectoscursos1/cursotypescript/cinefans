interface Service {
    Renew(): void;
    Login(): void;
    Cancel(): void;
}

class Netflix implements Service {
    Renew(): void {
        console.log('Renewing Netflix')
    }
    Login(): void {
        console.log('Login to Netflix')
    }
    Cancel(): void {
        console.log('Canceling Netflix')
    }
}

class AmazonPrime implements Service {
    Renew(): void {
        console.log('Renewing AmazonPrime')
    }
    Login(): void {
        console.log('Login to AmazonPrime')
    }
    Cancel(): void {
        console.log('Canceling AmazonPrime')
    }
}

class HBO implements Service {
    Renew(): void {
        console.log('Renewing HBO')
    }
    Login(): void {
        console.log('Login to HBO')
    }
    Cancel(): void {
        console.log('Canceling HBO')
    }
}

enum ServiceType {
    Renew,
    Login,
    Cancel
}

const button = document.getElementById('btn')!;
const service = <HTMLSelectElement>document.getElementById('service');
const task = <HTMLSelectElement>document.getElementById('task');

let selectService: Service;
let currentOperation: ServiceType;

service.addEventListener('change', () => {
    switch (service.value) {
        case '1':
            selectService = new Netflix();
            break;
        case '2':
            selectService = new AmazonPrime();
            break;
        case '3':
            selectService = new HBO();
            break;
    }
});

task.addEventListener('change', () => {
    switch (task.value) {
        case '1':
            currentOperation = ServiceType.Renew;
            break;
        case '2':
            currentOperation = ServiceType.Login;
            break;
        case '3':
            currentOperation = ServiceType.Cancel;
            break;
    }
});

button.addEventListener('click', () => {
    switch (currentOperation) {
        case ServiceType.Renew: selectService.Renew();
            break;

        case ServiceType.Login: selectService.Login();
            break;

        case ServiceType.Cancel: selectService.Cancel();
            break;
    }
})
