"use strict";
class Netflix {
    Renew() {
        console.log('Renewing Netflix');
    }
    Login() {
        console.log('Login to Netflix');
    }
    Cancel() {
        console.log('Canceling Netflix');
    }
}
class AmazonPrime {
    Renew() {
        console.log('Renewing AmazonPrime');
    }
    Login() {
        console.log('Login to AmazonPrime');
    }
    Cancel() {
        console.log('Canceling AmazonPrime');
    }
}
class HBO {
    Renew() {
        console.log('Renewing HBO');
    }
    Login() {
        console.log('Login to HBO');
    }
    Cancel() {
        console.log('Canceling HBO');
    }
}
var ServiceType;
(function (ServiceType) {
    ServiceType[ServiceType["Renew"] = 0] = "Renew";
    ServiceType[ServiceType["Login"] = 1] = "Login";
    ServiceType[ServiceType["Cancel"] = 2] = "Cancel";
})(ServiceType || (ServiceType = {}));
const button = document.getElementById('btn');
const service = document.getElementById('service');
const task = document.getElementById('task');
let selectService;
let currentOperation;
service.addEventListener('change', () => {
    switch (service.value) {
        case '1':
            selectService = new Netflix();
            break;
        case '2':
            selectService = new AmazonPrime();
            break;
        case '3':
            selectService = new HBO();
            break;
    }
});
task.addEventListener('change', () => {
    switch (task.value) {
        case '1':
            currentOperation = ServiceType.Renew;
            break;
        case '2':
            currentOperation = ServiceType.Login;
            break;
        case '3':
            currentOperation = ServiceType.Cancel;
            break;
    }
});
button.addEventListener('click', () => {
    switch (currentOperation) {
        case ServiceType.Renew:
            selectService.Renew();
            break;
        case ServiceType.Login:
            selectService.Login();
            break;
        case ServiceType.Cancel:
            selectService.Cancel();
            break;
    }
});
